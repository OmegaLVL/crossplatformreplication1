#include "Rock.h"
#include "Maths.h"
#include "InputState.h"
#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"

//zoom hardcoded at 100...if we want to lock players on screen, this could be calculated from zoom
const float HALF_WORLD_HEIGHT = 3.6f;
const float HALF_WORLD_WIDTH = 6.4f;

Rock::Rock() :
	GameObject(),
	mRockId(0)
{}


void Rock::Update()
{

}


void Rock::Write( OutputMemoryBitStream& inOutputStream) const
{
	
	inOutputStream.Write( GetRockId() );

	Vector3 location = GetLocation();
	inOutputStream.Write( location.mX );
	inOutputStream.Write( location.mY );

	inOutputStream.Write(GetColor());
}

bool Rock::operator==(Rock &other)
{
	// Game Object Part.
	//Call the == of the base, Player reference is
	//downcast explicitly.
	if(!GameObject::operator==(other)) return false;

	if(this->mRockId != other.mRockId) return false;

	return true;
}

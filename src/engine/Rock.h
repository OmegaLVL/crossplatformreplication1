#ifndef ROCK_H_
#define ROCK_H_

#include "GameObject.h"
#include "World.h"

class InputState;

/* We'll later create client and server versions of this class */

class Rock : public GameObject
{
public:
	CLASS_IDENTIFICATION( 'ROCK', GameObject )
	
	static	GameObject*	StaticCreate()			{ return new Rock(); }

	//Note - the code in the book doesn't provide this until the client.
	//This however limits testing.
	static	GameObjectPtr	StaticCreatePtr()			{ return GameObjectPtr(new Rock()); }
	
	virtual void Update() override;

	void		SetRockId( uint32_t inRockId )			{ mRockId = inRockId; }
	uint32_t	GetRockId()						const 	{ return mRockId; }

	
//	virtual void	Read( InputMemoryBitStream& inInputStream ) override;

	void Write( OutputMemoryBitStream& inOutputStream) const override;
	
	bool operator==(Rock &other);

protected:
	Rock();

private:

	uint32_t			mRockId;
};

typedef shared_ptr< Rock >	RockPtr;

#endif // ROCK_H_

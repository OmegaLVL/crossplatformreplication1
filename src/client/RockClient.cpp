#include "RockClient.h"

#include "TextureManager.h"
#include "GameObjectRegistry.h"

#include "InputMemoryBitStream.h"
#include "OutputMemoryBitStream.h"
#include "NetworkManagerClient.h"

#include "StringUtils.h"

RockClient::RockClient()
{
	mSpriteComponent.reset( new SpriteComponent( this ) );
	mSpriteComponent->SetTexture( TextureManager::sInstance->GetTexture( "rock" ) );
}


void RockClient::Update()
{
	//for now, we don't simulate any movement on the client side
	//we only move when the server tells us to move
}

void RockClient::Read( InputMemoryBitStream& inInputStream )
{
	
	uint32_t rockId;
	inInputStream.Read( rockId );
	SetRockId( rockId );
	

	Vector3 replicatedLocation;


	inInputStream.Read( replicatedLocation.mX );
	inInputStream.Read( replicatedLocation.mY );

	SetLocation( replicatedLocation );

	Vector3 color;
	inInputStream.Read( color );
	SetColor( color );
	
	
	if( GetRockId() == NetworkManagerClient::sInstance->GetRockId() )
	{
		//	HUD::sInstance->SetPlayerHealth( mHealth );
	}
}
